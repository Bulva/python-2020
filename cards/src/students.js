const students = [
    {
        "id": 1,
        "name": "Romana Drahošová",
        "points": 0,
        "drawed": false
    },
    {
        "id": 2,
        "name": "Jan Holub",
        "points": 0,
        "drawed": false
    },
    {
        "id": 3,
        "name": "David Kirner",
        "points": 0,
        "drawed": false
    },
    {
        "id": 4,
        "name": "Mária Kmošková",
        "points": 0,
        "drawed": false
    },
    {
        "id": 5,
        "name": "Ondřej Mucha",
        "points": 0,
        "drawed": false
    },
    {
        "id": 6,
        "name": "Veronika Nováková",
        "points": 0,
        "drawed": false
    },
    {
        "id": 7,
        "name": "Barbora Plačková",
        "points": 0,
        "drawed": false
    }
]
export default students;